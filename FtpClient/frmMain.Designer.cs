﻿namespace FtpClient
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listDirectoryContent = new System.Windows.Forms.ListView();
            this.btnLogout = new System.Windows.Forms.Button();
            this.menuFileOperations = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.downloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dialogFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.btnUpload = new System.Windows.Forms.Button();
            this.dialogFile = new System.Windows.Forms.OpenFileDialog();
            this.btnMkdir = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.menuFileOperations.SuspendLayout();
            this.SuspendLayout();
            // 
            // listDirectoryContent
            // 
            this.listDirectoryContent.LabelEdit = true;
            this.listDirectoryContent.Location = new System.Drawing.Point(12, 12);
            this.listDirectoryContent.Name = "listDirectoryContent";
            this.listDirectoryContent.Size = new System.Drawing.Size(525, 343);
            this.listDirectoryContent.TabIndex = 0;
            this.listDirectoryContent.UseCompatibleStateImageBehavior = false;
            this.listDirectoryContent.View = System.Windows.Forms.View.SmallIcon;
            this.listDirectoryContent.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.listDirectoryContent_AfterLabelEdit);
            this.listDirectoryContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listDirectoryContent_MouseClick);
            this.listDirectoryContent.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listDirectoryContent_MouseDoubleClick);
            // 
            // btnLogout
            // 
            this.btnLogout.Location = new System.Drawing.Point(562, 167);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 35);
            this.btnLogout.TabIndex = 1;
            this.btnLogout.Text = "Log out";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // menuFileOperations
            // 
            this.menuFileOperations.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.downloadToolStripMenuItem});
            this.menuFileOperations.Name = "menuFileOperations";
            this.menuFileOperations.Size = new System.Drawing.Size(129, 48);
            // 
            // downloadToolStripMenuItem
            // 
            this.downloadToolStripMenuItem.Name = "downloadToolStripMenuItem";
            this.downloadToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.downloadToolStripMenuItem.Text = "Download";
            this.downloadToolStripMenuItem.Click += new System.EventHandler(this.downloadToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(562, 44);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(75, 35);
            this.btnUpload.TabIndex = 2;
            this.btnUpload.Text = "Upload file";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnMkdir
            // 
            this.btnMkdir.Location = new System.Drawing.Point(562, 85);
            this.btnMkdir.Name = "btnMkdir";
            this.btnMkdir.Size = new System.Drawing.Size(75, 35);
            this.btnMkdir.TabIndex = 3;
            this.btnMkdir.Text = "Create directory";
            this.btnMkdir.UseVisualStyleBackColor = true;
            this.btnMkdir.Click += new System.EventHandler(this.btnMkdir_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(562, 126);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 35);
            this.btnRefresh.TabIndex = 4;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 367);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnMkdir);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.btnLogout);
            this.Controls.Add(this.listDirectoryContent);
            this.Name = "frmMain";
            this.Text = "FTP Client";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.menuFileOperations.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listDirectoryContent;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.ContextMenuStrip menuFileOperations;
        private System.Windows.Forms.ToolStripMenuItem downloadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog dialogFolder;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.OpenFileDialog dialogFile;
        private System.Windows.Forms.Button btnMkdir;
        private System.Windows.Forms.Button btnRefresh;
    }
}

