﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace FtpClient
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            try
            {
                Program.client = new FtpClient(tbPassword.Text, tbUsername.Text, tbUri.Text);
                Program.mainForm.ClearListView();              
                Program.mainForm.FillListView(Program.client.GetServerContent());
                Program.mainForm.Visible = true;
                Visible = false;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Cant connect to the server");
            }

        }

        
    }
}
