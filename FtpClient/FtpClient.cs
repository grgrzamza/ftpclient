﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using EnterpriseDT.Net.Ftp;
using System.Text.RegularExpressions;

namespace FtpClient
{
    public struct FileInfo
    {
        public string name;
        public string size;
        public string type;
    }

    class FtpClient
    {
        public string UserName { get; private set; }
        public string Uri { get; private set; }
        private string password;

        private int bufferSize = 1024;

        public FtpClient(string password, string userName, string uri)
        {
            this.password = password;
            UserName = userName;
            Uri = uri;
        }

        private string GetPath(string uri, string path)
        {
            return Path.Combine(uri, path).Replace("\\", "/");
        }

        public string ChangeWorkingDirectory(string path)
        {
            Uri = GetPath(Uri, path);

            return PrintWorkingDirectory();
        }

        public string PrintWorkingDirectory()
        {
            var request = CreateRequest(Uri, WebRequestMethods.Ftp.PrintWorkingDirectory);

            return GetStatusDescription(request);
        }

        private FtpWebRequest CreateRequest(string uri, string method)
        {
            var request = (FtpWebRequest)WebRequest.Create(uri);

            if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(password))
            {
                request.Credentials = new NetworkCredential(UserName, password);
            }
            request.Method = method;
            request.UseBinary = true;
            request.EnableSsl = false;
            request.UsePassive = true;

            return request;
        }

        private string GetStatusDescription(FtpWebRequest request)
        {
            using (var response = (FtpWebResponse)request.GetResponse())
            {
                return response.StatusDescription;
            }
        }

        public string MakeDirectory(string directoryName)
        {
            var request = CreateRequest(GetPath(Uri, directoryName), WebRequestMethods.Ftp.MakeDirectory);

            return GetStatusDescription(request);
        }

        public string RemoveDirectory(string directoryName)
        {
            var request = CreateRequest(GetPath(Uri, directoryName), WebRequestMethods.Ftp.RemoveDirectory);

            return GetStatusDescription(request);
        }

        public string Rename(string currentName, string newName)
        {
            var request = CreateRequest(GetPath(Uri, currentName), WebRequestMethods.Ftp.Rename);

            request.RenameTo = newName;

            return GetStatusDescription(request);
        }

        public string DeleteFile(string fileName)
        {
            var request = CreateRequest(GetPath(Uri, fileName), WebRequestMethods.Ftp.DeleteFile);

            return GetStatusDescription(request);
        }

        public string DownloadFile(string source, string dest)
        {
            var request = CreateRequest(GetPath(Uri, source), WebRequestMethods.Ftp.DownloadFile);

            string filepath = GetPath(dest, Path.GetFileName(source));

            byte[] buffer = new byte[bufferSize];

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var fs = new FileStream(filepath, FileMode.OpenOrCreate))
                    {
                        int readCount = stream.Read(buffer, 0, bufferSize);

                        while (readCount > 0)
                        {
                            fs.Write(buffer, 0, readCount);
                            readCount = stream.Read(buffer, 0, bufferSize);
                        }
                    }
                }

                return response.StatusDescription;
            }
        }

        public long GetFileSize(string fileName)
        {
            var request = CreateRequest(GetPath(Uri, fileName), WebRequestMethods.Ftp.GetFileSize);

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                return response.ContentLength;
            }
        }

        public string UploadFile(string source, string dest)
        {                                   
            var request = CreateRequest(GetPath(Uri, dest), WebRequestMethods.Ftp.UploadFile);

            using (var stream = request.GetRequestStream())
            {
                using (var fileStream = File.Open(source, FileMode.Open))
                {
                    int num;

                    byte[] buffer = new byte[bufferSize];

                    while ((num = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {                    
                        stream.Write(buffer, 0, num);
                    }
                }
            }

            return GetStatusDescription(request);
        }

        public string UploadFileWithUniqueName(string source)
        {
            var request = CreateRequest(Uri, WebRequestMethods.Ftp.UploadFileWithUniqueName);

            using (var stream = request.GetRequestStream())
            {
                using (var fileStream = File.Open(source, FileMode.Open))
                {
                    int num;

                    byte[] buffer = new byte[bufferSize];

                    while ((num = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        stream.Write(buffer, 0, num);
                    }
                }
            }

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                return Path.GetFileName(response.ResponseUri.ToString());
            }
        }

        public List<string> ListDirectory()
        {
            var list = new List<string>();

            var request = CreateRequest(Uri, WebRequestMethods.Ftp.ListDirectory);

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        while (!reader.EndOfStream)
                        {
                            list.Add(reader.ReadLine());
                        }
                    }
                }
            }

            return list;
        }

        public List<string> ListDirectoryDetails()
        {
            var list = new List<string>();

            var request = CreateRequest(Uri, WebRequestMethods.Ftp.ListDirectoryDetails);

            using (var response = (FtpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        while (!reader.EndOfStream)
                        {
                            list.Add(reader.ReadLine());
                        }
                    }
                }
            }
            return list;
        }

        public List<FileInfo> GetServerContent()
        {
            Regex regex = new Regex(@"^([d-])([rwxt-]{3}){3}\s+\d{1,}\s+.*?(\d{1,})\s+(\w+\s+\d{1,2}\s+(?:\d{4})?)(\d{1,2}:\d{2})?\s+(.+?)\s?$",
                        RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            List<FileInfo> files = new List<FileInfo>();
            List<string> list = Program.client.ListDirectoryDetails();
            foreach (string path in list)
            {
                Match match = regex.Match(path);
                FileInfo file;
                if (match.Length > 5)
                {
                    if (match.Groups[1].Value == "d")
                    {
                        file.size = "";
                        file.name = match.Groups[6].Value;
                        file.type = "dir";
                    }
                    else
                    {
                        file.size = (Int32.Parse(match.Groups[3].Value.Trim()) / 1024).ToString() + " KB";
                        file.name = match.Groups[6].Value;
                        file.type = "file";
                    }
                    files.Add(file);
                }
            }
            files.Reverse();
            return files;
        }


    }
}
