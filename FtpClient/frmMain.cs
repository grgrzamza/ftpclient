﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FtpClient
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            InitListView();
        }        

        public delegate string RenameDelegate(string oldpath, string newpath);

        public void FillListView(List<FileInfo> list)
        {
            foreach(FileInfo file in list)
            {
                var item = new ListViewItem(new[] { file.name, file.size, file.type});
                listDirectoryContent.Items.Add(item);
            }                       
        }

        private void InitListView()
        {
            listDirectoryContent.View = View.Details;
            listDirectoryContent.Columns.Add("Name").Width = listDirectoryContent.Width / 2 - 2;
            listDirectoryContent.Columns.Add("Size").Width = listDirectoryContent.Width / 2 - 2;
        }

        public void ClearListView()
        {
            listDirectoryContent.Items.Clear();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.registrationForm.Close();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            Program.mainForm.Visible = false;
            Program.registrationForm.Visible = true;
        }

        private void listDirectoryContent_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listDirectoryContent.FocusedItem.Bounds.Contains(e.Location))
                {
                    if (listDirectoryContent.FocusedItem.SubItems[2].Text.Equals("dir"))
                    {
                        menuFileOperations.Items[1].Visible = false;
                    }
                    else
                    {
                        menuFileOperations.Items[1].Visible = true;
                    }
                    menuFileOperations.Show(Cursor.Position);
                }
            }
        }

        private void downloadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                dialogFolder.ShowDialog();
                if (!string.IsNullOrEmpty(dialogFolder.SelectedPath))
                Program.client.DownloadFile(listDirectoryContent.FocusedItem.SubItems[0].Text, dialogFolder.SelectedPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't download file");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (listDirectoryContent.FocusedItem.SubItems[2].Text.Equals("file"))
                {    
                    Program.client.DeleteFile(listDirectoryContent.FocusedItem.SubItems[0].Text);
                }
                else
                {
                    Program.client.RemoveDirectory(listDirectoryContent.FocusedItem.SubItems[0].Text);
                }
                Program.mainForm.ClearListView();
                Program.mainForm.FillListView(Program.client.GetServerContent());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't delete file");
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                dialogFile.ShowDialog();
                if (!string.IsNullOrEmpty(dialogFile.FileName))
                {
                    Program.client.UploadFile(dialogFile.FileName, Path.GetFileName(dialogFile.FileName));
                    Program.mainForm.ClearListView();
                    Program.mainForm.FillListView(Program.client.GetServerContent());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't upload file");
            }
        }

        private void listDirectoryContent_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listDirectoryContent.FocusedItem.Bounds.Contains(e.Location) && listDirectoryContent.FocusedItem.SubItems[2].Text.Equals("dir"))
            {
                Program.client.ChangeWorkingDirectory(listDirectoryContent.FocusedItem.SubItems[0].Text);
                Program.mainForm.ClearListView();
                Program.mainForm.FillListView(Program.client.GetServerContent());
            }
        }

        private void btnMkdir_Click(object sender, EventArgs e)
        {
            try
            {
                var item = new ListViewItem(new[] { "Directory", "", "dir" });
                listDirectoryContent.Items.Add(item);
                Program.client.MakeDirectory(item.SubItems[0].Text);
                ClearListView();
                FillListView(Program.client.GetServerContent());
                foreach (ListViewItem listItem in listDirectoryContent.Items)
                {
                    if (listItem.SubItems[0].Text.Equals("Directory") && listItem.SubItems[2].Text.Equals("dir"))
                    {
                        listItem.BeginEdit();
                        return;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Can't create directory.");
            }
        }

        private void listDirectoryContent_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            try
            {
                Program.client.Rename(listDirectoryContent.FocusedItem.SubItems[0].Text, e.Label);
                Program.mainForm.ClearListView();
                Program.mainForm.FillListView(Program.client.GetServerContent());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't rename file");
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Program.mainForm.ClearListView();
            Program.mainForm.FillListView(Program.client.GetServerContent());
        }
    }
}
